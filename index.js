'use strict';

const Hapi = require('@hapi/hapi');
const Joi = require('@hapi/joi');
const Boom = require('@hapi/boom');
const Vision = require('@hapi/vision');
const Inert = require('@hapi/inert');
const Path = require('path');

// config variables
const config = require('./config/config.js');

const rootHandler = (request, h) => {
    return h.view('index', {
        title: 'Loja' + request.server.version
    });
};

const init = async () => {

    const server = Hapi.server({
        port: global.gConfig.node_port,
        host: '0.0.0.0',
        routes: {
            files: {
                relativeTo: Path.join(__dirname, 'src')
            }
        }
    });

    await server.register([
        Vision,
        Inert,
        require('./services/default-service')('stock'),
        require('./services/default-service')('provider'),
        require('./services/pdv-service')()
    ]);

    server.views({
        engines: {
            pug: require('pug')
        },
        relativeTo: __dirname,
        path: 'templates',
        context: {
            brand: global.gConfig.brand
        }
    });

    server.route({
        method: 'GET',
        path: '/',
        handler: rootHandler
    });

    server.route({
        method: 'GET',
        path: '/{filename}',
        handler: (request, h) => {
            return h.view(request.params.filename, {
                title: 'Loja' + request.server.version
            });
        }
    });

    server.route({
        method: 'GET',
        path: '/{param*}',
        handler: {
            directory: {
                path: '.'
            }
        }
    });

    await server.start();
    console.log('Front running on %s', server.info.uri);
};

init();