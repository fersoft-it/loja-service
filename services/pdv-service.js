'use strict';

const Wreck = require('@hapi/wreck');
require ('../config/config');

const pdvService = (path='pdv') => {
    const base_url = global.gConfig[path+'_url'] + '/' + path;
    const base_path = '/'+path;
    return {
        name: path+'-plugin',
        version: '1.0.0',
        register: async (server, options) => {
            server.route({
                method: 'GET',
                path: base_path,
                handler: async function (request, h) {
                    console.log('calling ',path,' at ',base_url);
                    let list = [];
                    let basket = [];
                    return h.view(path, {
                        title: 'Loja' + request.server.version,
                        list: list,
                        basket: basket
                    });
                }
            })

            server.route({
                method: 'GET',
                path: base_path + '/edit',
                handler: {
                    view: {
                        template: path + '-edit',
                        context: {
                          name: path,
                           _item: {}
                        }
                    }
                }
            })
        }
    }
};

module.exports = pdvService