'use strict';

const Wreck = require('@hapi/wreck');
require ('../config/config');

const defaultService = (path) => {
    const base_url = global.gConfig[path+'_url'] + '/' + path;
    const base_path = '/'+path;
    return {
        name: path+'-plugin',
        version: '1.0.0',
        register: async function (server, options) {
            server.route({
                method: 'GET',
                path: base_path,
                handler: async function (request, h) {
                    console.log('calling ',path,' at ',base_url);
                    const promise = await Wreck.request('GET', base_url);
                    let body = [];
                    try {
                        body = await Wreck.read(promise, {
                            json: true
                        });
                        console.log(path + ' ITEMS: ', body)
                    } catch (ex){console.log('ERROR: ITEMS: ', ex)}
                    return h.view(path, {
                        title: 'Loja' + request.server.version,
                        list: body
                    });
                }
            })

            server.route({
                method: 'GET',
                path: base_path + '/edit',
                handler: {
                    view: {
                        template: path + '-edit',
                        context: {
                          name: path,
                           _item: {}
                        }
                    }
                }
            })

            server.route({
                method: 'POST',
                path: base_path + '/edit',
                handler: async (request, h) => {
                    let options = {
                        payload: request.payload
                    }

                    const api = request.payload.id ? base_url + '/' + request.payload.id : base_url;
                    const method = request.payload.id ? 'PUT' : 'POST';
                    try{
                        const promise = await Wreck.request(method, api, options);                        
                    }
                    catch(ex) {
                        console.log(err)
                    }

                    return h.redirect('/'+path)
                }
            })

            server.route({
                method: 'GET',
                path: base_path + '/{id}',
                handler: async (request, h) => {

                    const promise = await Wreck.request('GET', base_url + '/' + request.params.id);
                    let body = {};
                    try {
                        body = await Wreck.read(promise, {
                            json: true
                        });
                        console.log('ITEM: ', body)
                    } catch(ex) {console.log('ERROR: ITEM: ', ex)}
                    return h.view(path+'-edit', {
                        name: path,
                        title: 'Loja' + request.server.version,
                        _item: body
                    });
                }
            })
        }
    }
};

module.exports = defaultService